package com.uthfullo.vocabulary.Main;

import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.uthfullo.vocabulary.Labels.db.InternalDB;
import com.uthfullo.vocabulary.Labels.db.Labels;
import com.uthfullo.vocabulary.R;

public class MainActivity extends AppCompatActivity {

    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    InternalDB internalDB;
    public static int totalNoOfLabel = 10;
    String TAG = "MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new TabFragment()).commit();

        setDummyData();
    }

    void setDummyData(){
        internalDB = InternalDB.getDatabase(this);
        new addAsyncTask(internalDB).execute();

    }


    private static class addAsyncTask extends AsyncTask<Labels, Void, Void> {

        private InternalDB db;

        addAsyncTask(InternalDB internalDB) {
            db = internalDB;
        }

        @Override
        protected Void doInBackground(final Labels... params) {

            Log.e("MainActivity", "Total row: "+db.itemLabelModel().getRowNumbers());
            if(db.itemLabelModel().getRowNumbers() != 0){
                return null;
            }

            for(int i=0; i <totalNoOfLabel; i++){
                if(i==0){
                    Labels labels = new Labels("unlocked", 0, 0);
                    db.itemLabelModel().addLabel(labels);
                }else{
                    Labels labels = new Labels("unlocked", 0, 0);
                    db.itemLabelModel().addLabel(labels);
                }

            }
            return null;
        }

    }

}
