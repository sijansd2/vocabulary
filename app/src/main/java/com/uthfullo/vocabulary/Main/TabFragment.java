package com.uthfullo.vocabulary.Main;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uthfullo.vocabulary.Labels.LabelPageFragment;
import com.uthfullo.vocabulary.R;
import com.uthfullo.vocabulary.Utils.SplViewPager;
import com.uthfullo.vocabulary.favourite.FavouriteFragment;


public class TabFragment extends Fragment {

    public static TabLayout tabLayout;
    public static SplViewPager viewPager;
    public static int tab_page_count = 2;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

            View x =  inflater.inflate(R.layout.tab_layout,null);
            tabLayout = (TabLayout) x.findViewById(R.id.tabs);
            viewPager = (SplViewPager) x.findViewById(R.id.viewpager);

        viewPager.setAdapter(new MyAdapter(getChildFragmentManager()));

        tabLayout.post(new Runnable() {
            @Override
            public void run() {
                    tabLayout.setupWithViewPager(viewPager);
                setupTabIcons();
                   }
        });

        return x;

    }

    private void setupTabIcons(){

        tabLayout.getTabAt(0).setText("Practices");
        tabLayout.getTabAt(1).setText("Favourites");
    }

    class MyAdapter extends FragmentPagerAdapter {

        public MyAdapter(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position)
        {
          switch (position){
              case 0 : return new LabelPageFragment();
              case 1 : return new FavouriteFragment();
          }
        return null;
        }

        @Override
        public int getCount() {

            return tab_page_count;

        }

        @Override
        public CharSequence getPageTitle(int position) {

            return null;

        }

    }



}
