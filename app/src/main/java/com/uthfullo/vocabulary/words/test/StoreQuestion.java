package com.uthfullo.vocabulary.words.test;

/**
 * Created by sijansd on 3/23/16.
 */
public class StoreQuestion {

    String _ques = "";
    String _ans1 = "";
    String _ans2 = "";
    String _ans3 = "";
    String _ans4 = "";
    String _rightAns = "";
    String _selectAns = "";

    int isSelecedId = 0;



    public void set_ques(String ques){
        _ques = ques;
    }
    public String get_ques() {
        return _ques;
    }

    public void set_ans1(String ans){
        _ans1 = ans;
    }
    public String get_ans1() {
        return _ans1;
    }

    public void set_ans2(String ans){
        _ans2 = ans;
    }
    public String get_ans2() {
        return _ans2;
    }

    public void set_ans3(String ans){
        _ans3 = ans;
    }
    public String get_ans3() {
        return _ans3;
    }

    public void set_ans4(String ans){
        _ans4 = ans;
    }
    public String get_ans4() {
        return _ans4;
    }

    public void set_rightAns(String ans){
        _rightAns = ans;
    }
    public String get_rightAns() {
        return _rightAns;
    }

    public void set_selectedAns(String ans){
        _selectAns = ans;
    }
    public String get_selectedAns() {
        return _selectAns;
    }

    public void setSelectedRadioButtonId(int ans){
        isSelecedId = ans;
    }
    public int getSelectedRadioButtonId() {
        return isSelecedId;
    }
}

