package com.uthfullo.vocabulary.words;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.uthfullo.vocabulary.R;
import com.uthfullo.vocabulary.favourite.FavouriteViewModel;
import com.uthfullo.vocabulary.words.db.vocabs;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LearningFragment extends Fragment {

    @Bind(R.id.parentLayout)RelativeLayout parentLayout;
    @Bind(R.id.viewPager)ViewPager pager;
    @Bind(R.id.next)Button nextBtn;
    @Bind(R.id.previous)Button previousBtn;
    //private PageIndicatorView pageIndicatorView;
    WordsViewModel wordsViewModel;
    FavouriteViewModel favouriteViewModel;
    boolean isReadyToLoadData = true;
    int position = -1;
    String TAG = "LearningFragment";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_learning, null);
        ButterKnife.bind(this, view);

        favouriteViewModel = ViewModelProviders.of(getActivity()).get(FavouriteViewModel.class);
        wordsViewModel = ViewModelProviders.of(this).get(WordsViewModel.class);


        int labelNo = getArguments().getInt("labelNo");

        //Load all favourite vocabs
        if(labelNo == 0){
            position = getArguments().getInt("position");
            favouriteViewModel.getFavouriteItemList().observe(this, new Observer<List<vocabs>>() {
                @Override
                public void onChanged(@Nullable List<vocabs> vocabs) {
                    if (isReadyToLoadData)
                    initViews(vocabs);
                }
            });
        }
        else {
            wordsViewModel.getWordsForLabel(labelNo).observe(this, new Observer<List<vocabs>>() {
                @Override
                public void onChanged(@Nullable List<vocabs> vocabs) {
                    if (isReadyToLoadData)
                        initViews(vocabs);
                }
            });
        }

        return view;
    }


    @SuppressWarnings("ConstantConditions")
    private void initViews(List<vocabs> data) {
        WordAdapter adapter = new WordAdapter();
        adapter.setData(createPageList(data));

        pager.setAdapter(adapter);

        if(position != -1)
        pager.setCurrentItem(position);

        if(pager.getCurrentItem() == 0){
            previousBtn.setVisibility(View.GONE);
            nextBtn.setVisibility(View.VISIBLE);
        }
        else if(pager.getCurrentItem() == data.size()-1) {
            previousBtn.setVisibility(View.VISIBLE);
            nextBtn.setVisibility(View.GONE);
        }
        else{
            previousBtn.setVisibility(View.VISIBLE);
            nextBtn.setVisibility(View.VISIBLE);
        }

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "page position: "+position);
                if(position == 0){
                    previousBtn.setVisibility(View.GONE);
                    nextBtn.setVisibility(View.VISIBLE);
                }
                else if(position == data.size()-1) {
                    previousBtn.setVisibility(View.VISIBLE);
                    nextBtn.setVisibility(View.GONE);
                }
                else{
                    previousBtn.setVisibility(View.VISIBLE);
                    nextBtn.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //pageIndicatorView = findViewById(R.id.pageIndicatorView);
        //pageIndicatorView.setViewPager(pager);
    }

    @NonNull
    private List<View> createPageList(List<vocabs> data) {
        List<View> pageList = new ArrayList<>();

        for(int i=0; i<data.size(); i++){
            pageList.add(createPageView(data.get(i)));
        }


        return pageList;
    }

    @NonNull
    private View createPageView(vocabs item) {

        View view = View.inflate(getActivity(), R.layout.word_detail_view, null);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT);
        view.setLayoutParams(params);

        TextView word = view.findViewById(R.id.word);
        word.setText(item.getWord());

        TextView meaning = view.findViewById(R.id.meaning);
        meaning.setText(item.getMeaning());

        TextView synonyms = view.findViewById(R.id.synonyms);
        synonyms.setText(item.getSynonyms());

        TextView antonyms = view.findViewById(R.id.antonyms);
        antonyms.setText(item.getAntonyms());

        ImageView favBtn = view.findViewById(R.id.favouriteBtn);

        if(item.getIsread() == 0){
            favBtn.setImageResource(R.drawable.ic_heart);
        }
        else {
            favBtn.setImageResource(R.drawable.ic_heart_red);
        }

        favBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isReadyToLoadData = false;
                if(item.getIsread() == 0){
                    favBtn.setImageResource(R.drawable.ic_heart_red);
                    item.setIsread(1);
                    wordsViewModel.updateWord(item);
                }
                else {
                    favBtn.setImageResource(R.drawable.ic_heart);
                    item.setIsread(0);
                    wordsViewModel.updateWord(item);
                }

            }
        });

        return view;
    }

    @OnClick(R.id.next)
    void nextClicked(){
        pager.setCurrentItem(pager.getCurrentItem()+1);
    }

    @OnClick(R.id.previous)
    void previousClicked(){
        pager.setCurrentItem(pager.getCurrentItem()-1);
    }
}
