package com.uthfullo.vocabulary.words.test;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.uthfullo.vocabulary.R;
import com.uthfullo.vocabulary.words.listener.TestListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by sijansd on 3/6/16.
 */

public class TestAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<StoreQuestion> quesList;
    public static ArrayList<HashMap<String,String>> listAns;
    public static String rb_select_ans = "rb";
    Context context;

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_FOOTER = 1;
    private static final int TYPE_ITEM = 2;

    boolean isResultPublished = false;
    TestListener listener;
    int labelNo = 0;

    public TestAdapter(Context context, int labelNo, List<StoreQuestion> data, TestListener listener) {
        quesList = data;
        this.context = context;
        this.listener = listener;
        this.labelNo = labelNo;

        listAns = new ArrayList<HashMap<String,String>>();
        for (int i=0; i<quesList.size(); i++){
            HashMap<String,String> row =  new HashMap<String, String>();
            row.put(rb_select_ans,"");
            this.listAns.add(row);
        }

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //Inflating recycle view item layout
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_test, parent, false);
            return new ItemViewHolder(itemView);
        } else if (viewType == TYPE_HEADER) {
            //Inflating header view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_list_header, parent, false);
            return new HeaderViewHolder(itemView);
        } else if (viewType == TYPE_FOOTER) {
            //Inflating footer view
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_list_footer, parent, false);
            return new FooterViewHolder(itemView);
        } else return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {


        if (holder instanceof HeaderViewHolder) {
            HeaderViewHolder headerHolder = (HeaderViewHolder) holder;
            if(isResultPublished){
                headerHolder.headerTitle.setText("Retest");
            }
            else{
                headerHolder.headerTitle.setText("Label No "+labelNo);
            }
            headerHolder.headerTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(context, "You clicked at Header View!", Toast.LENGTH_SHORT).show();
                    isResultPublished = false;
                    notifyDataSetChanged();
                }
            });
        } else if (holder instanceof FooterViewHolder) {
            FooterViewHolder footerHolder = (FooterViewHolder) holder;
            if(isResultPublished){
                footerHolder.footerText.setText("Show Result");
            }else{
                footerHolder.footerText.setText("DONE");
            }
            footerHolder.footerText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Toast.makeText(context, "You clicked at Footer View", Toast.LENGTH_SHORT).show();
                    listener.doneClicked();
                    isResultPublished = true;
                    notifyDataSetChanged();
                }
            });
        } else if (holder instanceof ItemViewHolder) {
            setItemViewHolder(holder, position);
        }



    }


    @Override
    public int getItemViewType(int position) {

        int footerPosition = quesList.size() + 1;
        if (position == 0) {
            return TYPE_HEADER;
        } else if (position == footerPosition) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return quesList.size()+2;
    }



    private class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView headerTitle;

        public HeaderViewHolder(View view) {
            super(view);
            headerTitle = (TextView) view.findViewById(R.id.header_text);
        }
    }

    private class FooterViewHolder extends RecyclerView.ViewHolder {
        TextView footerText;

        public FooterViewHolder(View view) {
            super(view);
            footerText = (TextView) view.findViewById(R.id.footer_text);
        }
    }

     class ItemViewHolder extends RecyclerView.ViewHolder {

        public RadioButton [] rb = new RadioButton[4];

        @Bind(R.id.tvWord)TextView tvWord;
        @Bind(R.id.card_view)CardView cv;
        @Bind(R.id.ansLay)LinearLayout ll;
        @Bind(R.id.rg)RadioGroup radioGroup;

        public ItemViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
            rb[0] = (RadioButton)v.findViewById(R.id.radioButton1);
            rb[1] = (RadioButton)v.findViewById(R.id.radioButton2);
            rb[2] = (RadioButton)v.findViewById(R.id.radioButton3);
            rb[3] = (RadioButton)v.findViewById(R.id.radioButton4);
        }
    }


    void setItemViewHolder(RecyclerView.ViewHolder itemViewHolder, int pos){

        int position = pos -1;
        ItemViewHolder holder = (ItemViewHolder) itemViewHolder;
        holder.tvWord.setText(quesList.get(position).get_ques());

        holder.rb[0].setText(quesList.get(position).get_ans1());
        holder.rb[1].setText(quesList.get(position).get_ans2());
        holder.rb[2].setText(quesList.get(position).get_ans3());
        holder.rb[3].setText(quesList.get(position).get_ans4());

        holder.radioGroup.setTag(position);

        holder.radioGroup.setOnCheckedChangeListener(null);
        holder.radioGroup.clearCheck();
        holder.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // TODO Auto-generated method stub
                int radioButtonID = group.getCheckedRadioButtonId();
                int clickedPos = (Integer) group.getTag();

                quesList.get(clickedPos).setSelectedRadioButtonId(radioButtonID);
                switch (checkedId) {
                    case R.id.radioButton1:
                        listAns.get(position).put(rb_select_ans, holder.rb[0].getText().toString());
                        break;
                    case R.id.radioButton2:
                        listAns.get(position).put(rb_select_ans, holder.rb[1].getText().toString());
                        break;
                    case R.id.radioButton3:
                        listAns.get(position).put(rb_select_ans, holder.rb[2].getText().toString());
                        break;
                    case R.id.radioButton4:
                        listAns.get(position).put(rb_select_ans, holder.rb[3].getText().toString());
                        break;

                }
            }
        });

        holder.radioGroup.check(quesList.get(position).getSelectedRadioButtonId());
        holder.itemView.setTag(quesList);

        //if(isResultPublished){
            for(int i=0; i<4; i++){
                holder.rb[i].setEnabled(!isResultPublished);
            }
        //}

//        if(isResultPublished) {
//            if (holder.rb[0].getId() == quesList.get(position).getSelectedRadioButtonId()) {
//
//                Drawable img = context.getResources().getDrawable(R.drawable.ic_tick);
//                img.setBounds(0, 0, 80, 80);
//
//                holder.rb[0].setCompoundDrawables(null, null, img, null);
//            }
//        }
    }

}