package com.uthfullo.vocabulary.words.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class vocabs {

    @PrimaryKey(autoGenerate = true)
    public int id;
    private String word;
    private String meaning;
    private String synonyms;
    private String antonyms;
    private int label_no = 0;
    private int isread = 0;

    public vocabs(String word, String meaning, String synonyms, String antonyms, int isread){

        this.word = word;
        this.meaning = meaning;
        this.synonyms = synonyms;
        this.antonyms = antonyms;
        this.isread = isread;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public void setSynonyms(String synonyms) {
        this.synonyms = synonyms;
    }

    public void setAntonyms(String antonyms) {
        this.antonyms = antonyms;
    }

    public void setIsread(int isread) {
        this.isread = isread;
    }

    public void setLabel_no(int label_no) {
        this.label_no = label_no;
    }


    public int getId() {
        return id;
    }

    public String getWord() {
        return word;
    }

    public String getMeaning() {
        return meaning;
    }

    public String getSynonyms() {
        return synonyms;
    }

    public String getAntonyms() {
        return antonyms;
    }

    public int getIsread() {
        return isread;
    }

    public int getLabel_no() {
        return label_no;
    }


}
