package com.uthfullo.vocabulary.words.test;

/**
 * Created by Sijansd on 3/15/2016.
 */
public class QuestionClass {

    // # for meaning
    // $ for synonym
    // @ for antonym
    // * for word

    public static String clue_word = "*";
    public static String clue_meaning = "#";
    public static String clue_synonym = "$";
    public static String clue_antonym = "@";

    public static String ques[] = {"What is the right meaning of "+clue_word+"?",
                     "Which is the right word of the synonyms "+clue_synonym+"?",
                     "Choose the right word of the meaning "+clue_meaning+".",
                     "Which is the right antonym of "+clue_word+"?"};

    public static String meaningType = "the right meaning";
    public static String wordType = "the right word";
    public static String synonymType = "the right synonym";
    public static String antonymType = "the right antonym";
}
