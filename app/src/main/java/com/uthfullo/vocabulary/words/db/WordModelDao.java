package com.uthfullo.vocabulary.words.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


/**
 * Created by macbookpro on 3/13/18.
 */

@Dao
public interface WordModelDao {

    @Query("select * from vocabs")
    LiveData<List<vocabs>> getAllWords();

    @Query("select * from vocabs where label_no = :label_no")
    LiveData<List<vocabs>> getWordsForLabel(int label_no);

    @Query("select * from vocabs where isread = :isRead")
    LiveData<List<vocabs>> getAllFavouriteWords(int isRead);

    @Query("select * from vocabs where id = :id")
    vocabs getItembyId(String id);

    @Insert(onConflict = REPLACE)
    void addWord(vocabs vocabs);

    @Delete
    void deleteWord(vocabs vocabs);

    @Update
    void updateWord(vocabs vocabs);
}
