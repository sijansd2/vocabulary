package com.uthfullo.vocabulary.words.test;

import android.app.AlertDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uthfullo.vocabulary.Labels.LabelViewModel;
import com.uthfullo.vocabulary.Labels.db.Labels;
import com.uthfullo.vocabulary.Main.MainActivity;
import com.uthfullo.vocabulary.R;
import com.uthfullo.vocabulary.words.WordsViewModel;
import com.uthfullo.vocabulary.words.db.AppDatabase;
import com.uthfullo.vocabulary.words.db.vocabs;
import com.uthfullo.vocabulary.words.listener.TestListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by macbookpro on 4/4/18.
 */

public class TestFragment extends Fragment{

    //List<StoreQuestion> quesList;
    AppDatabase appDatabase;
    WordsViewModel wordsViewModel;
    LabelViewModel labelViewModel;
    private RecyclerView.Adapter mAdapter;

    @Bind(R.id.exam_recycler_view)RecyclerView mRecyclerView;
    List<vocabs> itemList;
    List<StoreQuestion> quesList;
    String TAG = "TestFragment";
    public int labelNo = 0;
    int passMark = 50;
    int percentage = 0;
    int rightAnsNo = 0;
    String resultStatus = "";
    //TestListener testListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test, null);
        ButterKnife.bind(this, view);
        //testListener = this;
        labelNo = getArguments().getInt("labelNo");

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        labelViewModel = ViewModelProviders.of(this).get(LabelViewModel.class);

        wordsViewModel = ViewModelProviders.of(this).get(WordsViewModel.class);
        wordsViewModel.getWordsForLabel(labelNo).observe(this, new Observer<List<vocabs>>() {
            @Override
            public void onChanged(@Nullable List<vocabs> vocabs) {
                initQuestionView(vocabs);
            }
        });

        return view;
    }


    private void initQuestionView(List<vocabs> vocabs){

        itemList = vocabs;
        quesList = new ArrayList<>();

        Random random = new Random();
            int max = QuestionClass.ques.length-1;
            int maxAns = 3;
            int min = 0;

            String ques = new String() ;
            String ans[] = new String[4];
            String rightAns = "";

            for(int i=0; i<itemList.size(); i++){

                int quesRandNum = random.nextInt(max - min + 1) + min;
                int ansRandNum = random.nextInt(maxAns - min + 1) + min;
                int otherPos = i+1;
                if(otherPos >= 5){
                    otherPos = 1;
                }

                ques = QuestionClass.ques[quesRandNum];

                //Question filter
                if(ques.contains(QuestionClass.clue_word)){
                    ques = ques.replace(QuestionClass.clue_word,"'"+itemList.get(i).getWord()+"'");
                }
                else if(ques.contains(QuestionClass.clue_meaning)){
                    ques = ques.replace(QuestionClass.clue_meaning,"("+itemList.get(i).getMeaning()+")");
                }
                else if(ques.contains(QuestionClass.clue_synonym)){
                    ques = ques.replace(QuestionClass.clue_synonym,"("+itemList.get(i).getSynonyms()+")");
                }
//                else if(ques.contains(QuestionClass.clue_antonym)){
//                    if(!itemList.get(i).get_antonym().equalsIgnoreCase("none"))
//                    ques = ques.replace(QuestionClass.clue_antonym,"'"+itemList.get(i).get_antonym()+"'");
//                }

                //Answer filter
                if(ques.contains(QuestionClass.wordType)) {
                    for (int j = 0; j < 4; j++) {
                        if (ansRandNum == j) {
                            ans[j] = itemList.get(i).getWord();
                            rightAns = ans[j];
                        }
                        else
                            ans[j] = itemList.get(otherPos++).getWord();
                    }
                }
                else if(ques.contains(QuestionClass.meaningType)) {
                    for (int j = 0; j < 4; j++) {
                        if (ansRandNum == j) {
                            ans[j] = itemList.get(i).getMeaning();
                            rightAns = ans[j];
                        }
                        else
                            ans[j] = itemList.get(otherPos++).getMeaning();
                    }
                }
                if(ques.contains(QuestionClass.synonymType)) {
                    for (int j = 0; j < 4; j++) {
                        if (ansRandNum == j) {
                            ans[j] = itemList.get(i).getSynonyms();
                            rightAns = ans[j];
                        }
                        else
                            ans[j] = itemList.get(otherPos++).getSynonyms();
                    }
                }
                if(ques.contains(QuestionClass.antonymType)) {
                    for (int j = 0; j < 4; j++) {
                        if (ansRandNum == j) {
                            ans[j] = itemList.get(i).getAntonyms();
                            rightAns = ans[j];
                        }
                        else
                            ans[j] = itemList.get(otherPos++).getAntonyms();
                    }
                }


                StoreQuestion sq = new StoreQuestion();
                sq.set_ques((i+1)+". "+ ques);
                sq.set_ans1(ans[0]);
                sq.set_ans2(ans[1]);
                sq.set_ans3(ans[2]);
                sq.set_ans4(ans[3]);
                sq.set_rightAns(rightAns);

                quesList.add(sq);
            }

            //progressBar.setVisibility(View.GONE);
            mAdapter = new TestAdapter(getActivity(),labelNo, quesList, new TestListener() {
                @Override
                public void doneClicked() {
                    show_result();
                }
            });
            mRecyclerView.setAdapter(mAdapter);
    }


    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    //Show result dlg
    public void show_result(){
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View dialogLayout = inflater.inflate(R.layout.dlg_result, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
        builder.setView(dialogLayout);
        final AlertDialog alertDialog = builder.show();
        alertDialog.show();

        ListView lv =  alertDialog.findViewById(R.id.listView);
        Button showResult = alertDialog.findViewById(R.id.showResult);
        TextView title = alertDialog.findViewById(R.id.resTitle);
        TextView desc = alertDialog.findViewById(R.id.resDesc);
        ImageView imageView = alertDialog.findViewById(R.id.imageView);
        //int wrongAnsNo = 0;

        for(int i=0; i<TestAdapter.listAns.size(); i++){

            if(quesList.get(i).get_rightAns().equals(TestAdapter.listAns.get(i).get(TestAdapter.rb_select_ans))){
                quesList.get(i).set_selectedAns("Ans: "+quesList.get(i).get_rightAns());
                rightAnsNo++;
            }
            else if( !TestAdapter.listAns.get(i).get(TestAdapter.rb_select_ans).equals("")){
                quesList.get(i).set_selectedAns("Correct Ans: "+quesList.get(i).get_rightAns());
                //wrongAnsNo++;
            }
        }

        lv.setAdapter(new ResultAdapter(getActivity().getApplicationContext(),quesList));

        percentage = (rightAnsNo * 100)/ TestAdapter.listAns.size();
        Log.e(TAG, "Percentage: "+percentage+"%");

        if(percentage>=passMark){
            title.setText("PASSED");
            imageView.setImageResource(R.drawable.ic_tick);
            String description = getActivity().getResources().getString(R.string.resultPassed);
            description = description.replace("*p*",percentage+"");
            desc.setText(description);
            resultStatus = "PASSED";
        }
        else {
            title.setText("FAILED");
            imageView.setImageResource(R.drawable.ic_cross);
            String description = getActivity().getResources().getString(R.string.resultFailed);
            description = description.replace("*p*",percentage+"");
            desc.setText(description);
            resultStatus = "FAILED";
        }



        Button okBtn = alertDialog.findViewById(R.id.dialogButtonOK);
        okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if(percentage >= passMark){
                    getActivity().finish();
                }
            }
        });

        alertDialog.findViewById(R.id.showResult).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lv.setVisibility(View.VISIBLE);
                okBtn.setVisibility(View.VISIBLE);

                title.setVisibility(View.GONE);
                desc.setVisibility(View.GONE);
                imageView.setVisibility(View.GONE);
                showResult.setVisibility(View.GONE);
            }
        });


        updateLabel(rightAnsNo, percentage, resultStatus);

    }

    void updateLabel(int marks, int percentage, String resultStatus){

        //Labels labels = labelViewModel.getItemById(labelNo);
        labelViewModel.getItemById(labelNo).observe(this, new Observer<Labels>() {
            @Override
            public void onChanged(@Nullable Labels labels) {

                labels.setMarks(marks);
                labels.setPercentage(percentage);
                labels.setLockStatus(resultStatus);

                labelViewModel.updateLabel(labels);
            }
        });

        if(percentage >= passMark && labelNo < MainActivity.totalNoOfLabel) {

            labelViewModel.getItemById(labelNo+1).observe(this, new Observer<Labels>() {
                @Override
                public void onChanged(@Nullable Labels labels) {
                    labels.setLockStatus("unlocked");
                    labelViewModel.updateLabel(labels);
                }
            });

        }


    }

}
