package com.uthfullo.vocabulary.words;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.uthfullo.vocabulary.R;
import com.uthfullo.vocabulary.words.test.TestFragment;

import butterknife.ButterKnife;


public class WordActivity extends AppCompatActivity {

    FragmentManager mFragmentManager;
    FragmentTransaction mFragmentTransaction;
    MenuItem itemTest;
    Bundle bundle;
    public int labelNo = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_words);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        labelNo = getIntent().getIntExtra("labelNo",1);
        int pos = getIntent().getIntExtra("position",-1);

        LearningFragment learningFragment = new LearningFragment();
        bundle = new Bundle();
        bundle.putInt("labelNo", labelNo);
        bundle.putInt("position", pos);
        learningFragment.setArguments(bundle);
        showFragment(learningFragment);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_word_activity, menu);
        itemTest = menu.findItem(R.id.test);

        if(labelNo == 0){
            itemTest.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.test:
                TestFragment testFragment = new TestFragment();
                if(bundle != null)
                testFragment.setArguments(bundle);
                showFragment(testFragment);
                itemTest.setVisible(false);
                break;
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        //Log.e("stake", mFragmentManager.getBackStackEntryCount()+"");
        if(mFragmentManager.getBackStackEntryCount() == 2){
            Log.e("stake", mFragmentManager.getBackStackEntryCount()+"");
            super.onBackPressed();
            itemTest.setVisible(true);
        }
        else if(mFragmentManager.getBackStackEntryCount() == 1){
            Log.e("stake", mFragmentManager.getBackStackEntryCount()+"");
            finish();
        }else {

        }

    }

    public void showFragment(Fragment fragment){
        //startActivity(new Intent(this, TestActivity.class));
        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView, fragment).commit();
        mFragmentTransaction.addToBackStack("aaa");
    }
}
