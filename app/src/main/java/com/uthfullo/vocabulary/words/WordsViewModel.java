package com.uthfullo.vocabulary.words;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.uthfullo.vocabulary.words.db.AppDatabase;
import com.uthfullo.vocabulary.words.db.vocabs;

import java.util.List;

/**
 * Created by macbookpro on 2/7/18.
 */

public class WordsViewModel extends AndroidViewModel {


    private AppDatabase appDatabase;

    public WordsViewModel(@NonNull Application application) {
        super(application);
        appDatabase = AppDatabase.getDatabase(application);
    }

    public LiveData<vocabs> itemClicked(vocabs model){

        MutableLiveData<vocabs> liveItem = new MutableLiveData<>();
        liveItem.setValue(model);
        return liveItem;
    }


    public LiveData<List<vocabs>> getWordsForLabel(int num){
        LiveData<List<vocabs>>  vocabsLiveData = appDatabase.itemWordModel().getWordsForLabel(num);
        return vocabsLiveData;
    }

    public void updateWord(vocabs vocabs){
        new UpdateAsyncTask(appDatabase, vocabs).execute();
    }

    private static class UpdateAsyncTask extends AsyncTask<vocabs, Void, Void> {

        private AppDatabase db;
        private vocabs vocabs;

        UpdateAsyncTask(AppDatabase appDatabase, vocabs vocabs) {
            this.db = appDatabase;
            this.vocabs = vocabs;
        }

        @Override
        protected Void doInBackground(final vocabs... params) {
            db.itemWordModel().updateWord(vocabs);
            return null;
        }

    }

}
