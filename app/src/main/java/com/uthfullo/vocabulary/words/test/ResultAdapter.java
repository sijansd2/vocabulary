package com.uthfullo.vocabulary.words.test;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.uthfullo.vocabulary.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ResultAdapter extends BaseAdapter{
    List<StoreQuestion>  resultItems;
    ArrayList<HashMap<String,String>> list;
    Context context;
    int [] imageId;
    private static LayoutInflater inflater=null;

    public ResultAdapter(Context mainActivity, List<StoreQuestion> items) {
        // TODO Auto-generated constructor stub
        resultItems=items;
        context=mainActivity;
        list = TestAdapter.listAns;
        inflater = ( LayoutInflater )context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return resultItems.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView ques;
        TextView ans;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.item_result, null);
        holder.ques=(TextView) rowView.findViewById(R.id.ques);
        holder.ans=(TextView) rowView.findViewById(R.id.ans);
        holder.ques.setText(resultItems.get(position).get_ques());

        if(!resultItems.get(position).get_selectedAns().equals("")){
            holder.ans.setText(resultItems.get(position).get_selectedAns());

            if(resultItems.get(position).get_selectedAns().contains("Correct Ans")){
                holder.ques.setTextColor(Color.RED);
                holder.ans.setTextColor(Color.BLACK);
            }
            else {
                holder.ques.setTextColor(Color.BLACK);
                holder.ans.setTextColor(Color.BLACK);
            }
        }

        return rowView;
    }

}