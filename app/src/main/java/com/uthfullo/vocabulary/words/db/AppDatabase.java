package com.uthfullo.vocabulary.words.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.huma.room_for_asset.RoomAsset;

/**
 * Created by macbookpro on 3/13/18.
 */

@Database(entities = {vocabs.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            //INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "borrow_db").build();
            INSTANCE = RoomAsset.databaseBuilder(context.getApplicationContext(), AppDatabase.class,"vocabulary.sqlite").build();
        }
        return INSTANCE;
    }

    public abstract WordModelDao itemWordModel();

}