package com.uthfullo.vocabulary.Utils;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by silvanamaria on 1/11/16.
 */
public class SplViewPager extends ViewPager {

    public static boolean isEnabled;

    public SplViewPager(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
        this.isEnabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if(this.isEnabled){
            return super.onTouchEvent(ev);
        }
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {

        if(this.isEnabled){
            return super.onInterceptTouchEvent(ev);
        }
        return false;

    }

    public static void setPageingEnabled(boolean isEnable){
        SplViewPager.isEnabled = isEnable;
    }
}
