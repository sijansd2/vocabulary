package com.uthfullo.vocabulary.favourite;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import com.uthfullo.vocabulary.words.db.AppDatabase;
import com.uthfullo.vocabulary.words.db.vocabs;

import java.util.List;


public class FavouriteViewModel extends AndroidViewModel {

    Application application;
    AppDatabase appDatabase;

    public FavouriteViewModel(@NonNull Application application) {
        super(application);

        this.application = application;
        appDatabase = AppDatabase.getDatabase(application);
    }

    public LiveData<List<vocabs>> getFavouriteItemList(){

        LiveData<List<vocabs>> itemList = appDatabase.itemWordModel().getAllFavouriteWords(1);
        return itemList;
    }

    public LiveData<vocabs> itemClicked(vocabs item){
        MutableLiveData<vocabs> liveItem = new MutableLiveData<>();
        liveItem.setValue(item);

        return liveItem;
    }


}
