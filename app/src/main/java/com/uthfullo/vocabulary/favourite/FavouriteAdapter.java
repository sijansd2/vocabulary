package com.uthfullo.vocabulary.favourite;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.uthfullo.vocabulary.R;
import com.uthfullo.vocabulary.words.WordActivity;
import com.uthfullo.vocabulary.words.db.vocabs;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;

public class FavouriteAdapter extends RecyclerView.Adapter<FavouriteAdapter.RecyclerViewHolder> {

    private List<vocabs> favList;
    private View.OnClickListener clickListener;

    public FavouriteAdapter(List<vocabs> favList) {
        this.favList = favList;
        //this.clickListener = listener;

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_favourite, parent, false));
    }

    @Override
    public void onBindViewHolder(FavouriteAdapter.RecyclerViewHolder holder, int position) {

        vocabs voc = favList.get(position);
        holder.favouriteWord.setText(voc.getWord());
        holder.itemView.setTag(voc);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(holder.favouriteWord.getContext(), WordActivity.class);
                intent.putExtra("labelNo", 0);
                intent.putExtra("position", position);
                holder.favouriteWord.getContext().startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return favList.size();
    }

    public void addItems(List<vocabs> itemList) {
        this.favList = itemList;
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.favouriteWord) TextView favouriteWord;

        RecyclerViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}