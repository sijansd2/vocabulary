package com.uthfullo.vocabulary.Labels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.uthfullo.vocabulary.Labels.db.InternalDB;
import com.uthfullo.vocabulary.Labels.db.Labels;

import java.util.List;

/**
 * Created by macbookpro on 2/7/18.
 */

public class LabelViewModel extends AndroidViewModel {

    Application application;
    InternalDB internalDB;

    public LabelViewModel(@NonNull Application application) {
        super(application);

        this.application = application;
        internalDB = InternalDB.getDatabase(application);
    }

    public LiveData<Labels> getItemById(int id){
        LiveData<Labels> labelsLiveData = internalDB.itemLabelModel().getLabelsById(id+"");
        return labelsLiveData;
    }

    public LiveData<List<Labels>> getLabelItemList(){
        LiveData<List<Labels>> itemList = internalDB.itemLabelModel().getAllLabels();
        return itemList;
    }

    public LiveData<Labels> itemClicked(Labels item){
        MutableLiveData<Labels> liveItem = new MutableLiveData<>();
        liveItem.setValue(item);

        return liveItem;
    }



    public void updateLabel(Labels labels){
        //internalDB.itemLabelModel().updateLabel(labels);
        new UpdateAsyncTask(internalDB, labels).execute();
    }

    private static class ItemGetByIdAsyncTask extends AsyncTask<String, Void, Void> {

        private InternalDB db;
        private String id;

        ItemGetByIdAsyncTask(InternalDB appDatabase, String id) {
            this.db = appDatabase;
            this.id = id;
        }

        @Override
        protected Void doInBackground(final String... params) {
            db.itemLabelModel().getLabelsById(id);
            return null;
        }

    }

    private static class UpdateAsyncTask extends AsyncTask<Labels, Void, Void> {

        private InternalDB db;
        private Labels labels;

        UpdateAsyncTask(InternalDB appDatabase, Labels labels) {
            this.db = appDatabase;
            this.labels = labels;
        }

        @Override
        protected Void doInBackground(final Labels... params) {
            db.itemLabelModel().updateLabel(labels);
            return null;
        }

    }

}
