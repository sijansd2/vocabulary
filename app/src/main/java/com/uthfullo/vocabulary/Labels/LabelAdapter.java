package com.uthfullo.vocabulary.Labels;

import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.uthfullo.vocabulary.Labels.db.Labels;
import com.uthfullo.vocabulary.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LabelAdapter extends RecyclerView.Adapter<LabelAdapter.RecyclerViewHolder> {

    private List<Labels> labelLList;
    private View.OnClickListener clickListener;
    int[] rainbow;

    public LabelAdapter(List<Labels> labelLList, int[] rainbow, View.OnClickListener listener) {
        this.labelLList = labelLList;
        this.rainbow = rainbow;
        this.clickListener = listener;

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_home, parent, false));
    }

    @Override
    public void onBindViewHolder(LabelAdapter.RecyclerViewHolder holder, int position) {

        Labels labels = labelLList.get(position);
        //holder.tvName.setText(homePageModel.getName());
        holder.tvLabelNo.setBackgroundResource(R.drawable.circle_bg);
        GradientDrawable drawable = (GradientDrawable) holder.tvLabelNo.getBackground();

        if(position > 10){
            position = position % 10;

            if(position == 0){
                position = 10;
            }
        }
        drawable.setColor(rainbow[position]);

        if(labels.getLockStatus().equalsIgnoreCase("unlocked")){
            holder.imvLock.setVisibility(View.GONE);
            holder.examStatus.setVisibility(View.GONE);
        }else if(labels.getLockStatus().equalsIgnoreCase("locked")){
            holder.imvLock.setVisibility(View.VISIBLE);
            holder.examStatus.setVisibility(View.GONE);
        }else if(labels.getLockStatus().equalsIgnoreCase("PASSED")){
            holder.imvLock.setVisibility(View.GONE);
            holder.examStatus.setVisibility(View.VISIBLE);
            holder.examStatus.setText("PASSED");
            holder.examStatus.setTextColor(holder.examStatus.getContext().getResources().getColor(android.R.color.holo_green_light));
        }else if(labels.getLockStatus().equalsIgnoreCase("FAILED")){
            holder.examStatus.setVisibility(View.VISIBLE);
            holder.imvLock.setVisibility(View.GONE);
            holder.examStatus.setText("FAILED");
            holder.examStatus.setTextColor(holder.examStatus.getContext().getResources().getColor(android.R.color.holo_red_light));
        }

        holder.tvLabelNo.setText(labels.getId()+"");
        holder.pbText.setText(labels.getPercentage()+"%");
        holder.tvMark.setText("done: "+labels.getMarks());
        holder.pb.setProgress(Integer.valueOf(labels.getPercentage()));

        holder.itemView.setTag(labels);
        holder.itemView.setOnClickListener(clickListener);
    }


    @Override
    public int getItemCount() {
        return labelLList.size();
    }

    public void addItems(List<Labels> itemList) {
        this.labelLList = itemList;
        notifyDataSetChanged();
    }

    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textView2) TextView tvName;
        @Bind(R.id.tvLabelNo) TextView tvLabelNo;
        @Bind(R.id.tvMark) TextView tvMark;
        @Bind(R.id.examStatus) TextView examStatus;
        @Bind(R.id.percentageText) TextView pbText;
        @Bind(R.id.progressBar) ProgressBar pb;
        @Bind(R.id.imvLock)ImageView imvLock;

        RecyclerViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}