package com.uthfullo.vocabulary.Labels.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import java.util.List;
import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


@Dao
public interface LabelModelDao {


    @Query("select count(*) from Labels")
    int getRowNumbers();

    @Query("select * from Labels")
    LiveData<List<Labels>> getAllLabels();

    @Query("select * from Labels where id = :id")
    LiveData<Labels> getLabelsById(String id);

    @Insert(onConflict = REPLACE)
    void addLabel(Labels labels);

    @Delete
    void deletLabel(Labels labels);

    @Update
    void updateLabel(Labels labels);
}
