package com.uthfullo.vocabulary.Labels.db;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by macbookpro on 4/2/18.
 */

@Entity
public class Labels {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String lockStatus;
    private int marks;
    private int percentage;


    public Labels(String lockStatus, int marks, int percentage){
        this.lockStatus = lockStatus;
        this.marks = marks;
        this.percentage = percentage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLockStatus() {
        return lockStatus;
    }

    public void setLockStatus(String lockStatus) {
        this.lockStatus = lockStatus;
    }

    public int getMarks() {
        return marks;
    }

    public void setMarks(int marks) {
        this.marks = marks;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }
}
