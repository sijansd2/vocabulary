package com.uthfullo.vocabulary.Labels;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uthfullo.vocabulary.Labels.db.Labels;
import com.uthfullo.vocabulary.R;
import com.uthfullo.vocabulary.words.WordActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LabelPageFragment extends Fragment implements View.OnClickListener{

    @Bind(R.id.my_recycler_view) RecyclerView recyclerView;

    LabelViewModel viewModel;
    LabelAdapter labelAdapter;
    int[] rainbow;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_homepage, null);
        ButterKnife.bind(this,view);

        rainbow = getActivity().getResources().getIntArray(R.array.rainbow);

        labelAdapter = new LabelAdapter(new ArrayList<Labels>(), rainbow, this);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        recyclerView.setAdapter(labelAdapter);

        viewModel = ViewModelProviders.of(getActivity()).get(LabelViewModel.class);
        viewModel.getLabelItemList().observe(this, new Observer<List<Labels>>() {
            @Override
            public void onChanged(@Nullable List<Labels> labelList) {
                labelAdapter.addItems(labelList);
            }
        });
        return view;
    }

    @Override
    public void onClick(View view) {
        Labels labels = (Labels) view.getTag();
        viewModel.itemClicked(labels).observe(getActivity(), new Observer<Labels>() {
            @Override
            public void onChanged(@Nullable Labels item) {
                //Log.e("Position--",item.getId());
                if(!labels.getLockStatus().equalsIgnoreCase("locked")) {
                    Intent intent = new Intent(getActivity(), WordActivity.class);
                    intent.putExtra("labelNo", labels.getId());
                    getActivity().startActivity(intent);
                }
            }
        });
    }
}
