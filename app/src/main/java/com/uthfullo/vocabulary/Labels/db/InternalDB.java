package com.uthfullo.vocabulary.Labels.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

/**
 * Created by macbookpro on 4/2/18.
 */

@Database(entities = {Labels.class}, version = 1)
public abstract class InternalDB extends RoomDatabase {

    private static InternalDB INSTANCE;

    public static InternalDB getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), InternalDB.class, "internal_db").build();
            //INSTANCE = RoomAsset.databaseBuilder(context.getApplicationContext(), InternalDB.class,"vocabulary.sqlite").build();
        }
        return INSTANCE;
    }

    public abstract LabelModelDao itemLabelModel();

}